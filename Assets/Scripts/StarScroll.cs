using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarScroll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(-0.5f * Time.deltaTime, 0);

        if (transform.position.x < -20)
        {
            transform.position = new Vector3(20f, transform.position.y);
        }
    }
}
