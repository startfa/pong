
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Ball ball;

    public Paddle playerPaddle;
    public int playerScore { get; private set; }
    public Text playerScoreText;

    public Paddle computerPaddle;
    public int computerScore { get; private set; }
    public Text computerScoreText;

    public int winScore = 5;
    public GameObject winnerDialog;

    private void Start()
    {
        NewGame();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) {
            NewGame();
        }
    }

    public void NewGame()
    {
        SetPlayerScore(0);
        SetComputerScore(0);
        StartRound();
    }

    public void StartRound()
    {
        this.playerPaddle.ResetPosition();
        this.computerPaddle.ResetPosition();
        this.ball.ResetPosition();
        this.ball.AddStartingForce();
        winnerDialog.SetActive(false);
    }

    public void PlayerScores()
    {
        SetPlayerScore(this.playerScore + 1);

        if (this.playerScore >= winScore)
        {
            winnerDialog.transform.GetChild(0).GetComponent<Text>().text = "Player WIN";
            winnerDialog.SetActive(true);
            ball.ResetPosition();
        }
        else
        {
            StartRound();
        }
    }

    public void ComputerScores()
    {
        SetComputerScore(this.computerScore + 1);
        if (this.computerScore >= winScore)
        {
            winnerDialog.transform.GetChild(0).GetComponent<Text>().text = "CPU WIN";
            winnerDialog.SetActive(true);
            ball.ResetPosition();
        } 
        else
        {
            StartRound();
        }
    }

    public void GoToCredit()
    {
        SceneManager.LoadScene("03EndScene");
    }

    private void SetPlayerScore(int score)
    {
        this.playerScore = score;
        this.playerScoreText.text = score.ToString();
    }

    private void SetComputerScore(int score)
    {
        this.computerScore = score;
        this.computerScoreText.text = score.ToString();
    }

}
