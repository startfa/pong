using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour
{
    public float speed = 220f;
    public new Rigidbody2D rigidbody { get; private set; }

    private void Awake()
    {
        this.rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        ResetPosition();
    }

    public void ResetPosition()
    {
        this.rigidbody.velocity = Vector2.zero;
        this.rigidbody.position = Vector2.zero;
    }

    public void AddStartingForce()
    {
        
        float x = Random.value < 0.5f ? -1f : 1f;

      
        float y = Random.value < 1f ? Random.Range(-1.5f, -1f)
                                      : Random.Range(1f, 1.5f);

        Vector2 direction = new Vector2(x, y);
        this.rigidbody.AddForce(direction * this.speed);
    }

    public void AddForce(Vector2 force)
    {
        this.rigidbody.AddForce(force);
    }
}
