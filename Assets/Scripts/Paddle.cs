using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    public float speed;

    protected Rigidbody2D _rigidbody { get; private set; }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void ResetPosition()
    {
        this._rigidbody.velocity = Vector2.zero;
        this._rigidbody.position = new Vector2(this._rigidbody.position.x, 0f);
    }
}
